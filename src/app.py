
from flask import Flask, render_template, request, redirect
import sqlite3
import csv
import json
import datetime
import time
from contextlib import closing

# configuration
DATABASE = '../db/dummy.db'
SCHEMA = 'schema.sql'
# TEAM_DATA = '../data/OrgStructure.csv'
TEAM_DATA = '../data/SurveyTeamStructureforCallum.csv'
# CONVERSATION_DATA = '../data/DummyConversations.csv'
# CONVERSATION_DATA = "../data/responsesOutDate7.csv"
CONVERSATION_DATA = "../data/SurveyResponsesforCallum.csv"
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'

app = Flask(__name__)

@app.route('/index')
def index():
  # return("hello")
  return render_template("index.html")

@app.route('/form')
def convForm():
  todaysDate = time.strftime("%Y-%m-%d")
  return render_template('form.html', todaysDate=todaysDate)

@app.route('/data')
def data():
  return(getOrgStructure())

@app.route('/submit/', methods = ['POST'])
def submit():
  myTeamID = request.form['myTeamID']
  theirTeamID = request.form['theirTeamID']
  convDate = request.form['convDate']
  stateRating = request.form['stateRating']
  submitData = (str(convDate), str(convDate), int(myTeamID), int(theirTeamID), int(stateRating))
  print(submitData)
  with closing(connect_db()) as db:
    db.cursor().execute("INSERT INTO Conversation (DateSubmitted, DateConducted, SubmittingTeamID, TargetTeamID, Appraisal) VALUES (?, ?, ?, ?, ?);", submitData)
    db.commit()
  print(query_db("SELECT * FROM Conversation WHERE TargetTeamID = 1"))
  # query_db("INSERT INTO Conversation (DateSubmitted, DateConducted, SubmittingTeamID, TargetTeamID, Appraisal) VALUES (?, ?, ?, ?, ?)", submitData)

  return render_template('submit.html')

@app.route('/colourJS')
def colourJS():
  print("woooo")
  return(app.send_static_file('Color.js'))

# @app.route('/example')
# def: example():

def connect_db():
  return sqlite3.connect(DATABASE)

def init_db():
  with closing(connect_db()) as db:
      with app.open_resource(SCHEMA, mode='r') as f:
        db.cursor().executescript(f.read())
      # read our csv files and populate database
      # start with teams
      with open(TEAM_DATA,'rU') as teams: # `with` statement available in 2.5+
        # csv.DictReader uses first line in file for column headings by default
        dr = csv.DictReader(teams) # comma is default delimiter
        to_db = [(i['TeamID'], i['Description'], i['ParentTeamID'], i['TeamSize']) for i in dr]
        db.cursor().executemany("INSERT INTO Team (TeamID, Description, ParentTeamID, TeamSize) VALUES (?, ?, ?, ?);", to_db)
      
      with open(CONVERSATION_DATA, 'rU') as conversations:
        dr = csv.DictReader(conversations)
        to_db = [(i['DateSubmitted'], i['DateConducted'], i['SubmittingTeamID'], i['TargetTeamID'], i['Appraisal']) for i in dr]
        db.cursor().executemany("INSERT INTO Conversation (DateSubmitted, DateConducted, SubmittingTeamID, TargetTeamID, Appraisal) VALUES (?, ?, ?, ?, ?);", to_db)

      db.commit()

def query_db(query, args=(), one=False):
  cur = connect_db().cursor()
  cur.execute(query, args)
  r = [dict((cur.description[i][0], value) \
             for i, value in enumerate(row)) for row in cur.fetchall()]
  cur.connection.close()
  return (r[0] if r else None) if one else r

# returns current org structure as JSON nodes and links
def getOrgStructure():
  # teams = query_db("SELECT * FROM Team")
  teams = query_db(""" 
      SELECT ParentTeamID, TeamSize, TeamID, Description, IFNULL(AvgAppraisal, 0) as AvgAppraisal 
      FROM (SELECT * FROM Team) as t1
      LEFT JOIN (SELECT TargetTeamID, AVG(Appraisal) as AvgAppraisal FROM Conversation 
          GROUP BY TargetTeamID) as t2
      ON t1.TeamID = t2.TargetTeamID
    """)
  

  # conversationSummary = query_db("""SELECT * FROM (SELECT * FROM Team INNER JOIN
  #                                     (SELECT TargetTeamID AS TeamID, AVG(Appraisal) 
  #                                     FROM Conversation GROUP BY TargetTeamID)
  #                                     ON Team.TeamID Conversation.TeamID);""")
  # print(conversationSummary)
  conversation = query_db("SELECT * FROM Conversation")
  links = []
  for team in teams:
    if team["ParentTeamID"]:
      links.append({"source" : team["TeamID"], "target" : team["ParentTeamID"]})
  teamsJson = json.dumps({"nodes" : teams, "links" : links})
  return(teamsJson)
  
if __name__ == '__main__':
    init_db()
    app.run(debug = True)




