
drop table if exists Conversation;
create table Conversation (
  ConversationID integer primary key autoincrement,
  DateSubmitted text not null,
  DateConducted text not null,
  SubmittingTeamID integer not null,
  TargetTeamID integer not null,
  Appraisal integer not null
);

drop table if exists Team;
create table Team (
  TeamID integer primary key,
  Description text not null,
  ParentTeamID integer,
  TeamSize integer not null
);

