// Set up the plot window.
var margin = 80;
var w = 700 - 2 * margin, h = 500 - 2 * margin;
var svg = d3.select("#plot").append("svg")
                .attr("width", w + 2 * margin)
                .attr("height", h + 2 * margin)
                .append("svg:g")
                .attr("transform", "translate(" + margin + ", " + margin + ")");


// Load the data.
var callback = function (data) {
  // Insert the data points.
  svg.selectAll("text")
    .data(data)
    .enter()
    .append("text")
    .text(function(d) {return d.nodes})
};

d3.json("/data", callback);
